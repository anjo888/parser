<?php

class Api
{
    /**
     * @OA\Info(
     *     version="1.0",
     *     title="Parser images links"
     * )
     */
    /**
     * @OA\Get(
     *     path="/images",
     *     summary="Get links images",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *     @OA\Schema(
     *     @OA\Property(
     *         property="url",
     *         type="string"
     *                 ),
     *         example={"url": "http://okozorko.ru"}
     *             )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not found"
     *     ),
     *     @OA\Response(
     *         response= 400,
     *         description="Bad Request"
     *     ),
     * )
     */
    public function jsonApi($images)
    {
        $status = '200';
        $response = [];
        if (!isset($_GET['url'])) {
            $status = '404';
        } else {

            $url = $_GET['url'];

            if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
                $response = $images;
            } else {
                $response = 400;
            }
        }
        $result = array(
            'status' => $status,
            'response' => $response,
        );
        return json_encode($result);
    }
}