<?php

class  EditOutput implements Output
{

    protected $output;

    public function __construct(Output $link)
    {
        $this->output= $link;
    }

    public function getLinks($images)
    {
        return '<strong>' . $this->output->getLinks($images) . '</strong>';
    }

}