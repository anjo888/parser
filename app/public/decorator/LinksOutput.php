<?php

class LinksOutput implements Output
{
    public function getLinks($images)
    {
        uasort($images, function ($a, $b) {
            return $a['width'] > $b['width'] ? 1 : -1;
        });
        $result = '';
        foreach ($images as $val) {

            $result .= "<pre>" . $val['link'] . "<br> Ширина " . $val['width'] . "<br>";
        }
        return $result;
    }

}