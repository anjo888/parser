<?php

require 'vendor/autoload.php';
include_once 'services/ImageParser.php';
include_once 'services/DownloadContent.php';
include_once 'decorator/Output.php';
include_once 'decorator/LinksOutput.php';
include_once 'decorator/EditOutput.php';

$route = new \Klein\Klein();


$route->respond('GET', '/', function () {

    $url = "http://okozorko.ru";
    $parseUrl = (new DownloadContent())->parserUrl($url);
    $images = (new ImageParser())->parseImages($parseUrl);

    $outputLinks = new linksOutput();
    $editOutput = new EditOutput($outputLinks);
    echo $outputLinks->getLinks($images);
    echo $editOutput->getLinks($images);

});

$route->respond('GET', '/openapi', function () {

    $yaml = file_get_contents('api/openapi.yaml');
    echo $yaml;
});
$route->dispatch();
