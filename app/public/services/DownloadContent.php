<?php

use GuzzleHttp\Client;

class DownloadContent
{
    public function parserUrl($url)
    {
        $client = new Client();
        $content = $client
            ->get($url)
            ->getBody()
            ->getContents();

        $model_page = phpQuery::newDocument($content);
        return $images_link = $model_page->find('img');

    }

}