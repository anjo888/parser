<?php

class ImageParser
{
    

    public function parseImages($images)
    {
        foreach ($images as $img) {
            $link = $this->getLink($img);
            $size = $this->getSize($link);
            if ($size > 3000) {
                $width = $this->getWidth($link);
                if ($width > 10) {
                    $result[] = array('link' => $link, "width" => $width);
                }
            }
        }
        return $result;
    }

    private function getLink($img)
    {
        $link = pq($img)->attr('src');
        return $link;
    }

    private function getSize($link)
    {
        $url = filter_var($link, FILTER_SANITIZE_URL);

        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            $data = get_headers($link, true);
            $size = $data['Content-Length'];
        }
        return $size;
    }

    private function getWidth($link)
    {
        $size = getimagesize($link);
        $width = $size[0];

        return $width;

    }

}
